


    
l = [1, 2, 3, 4, 5]
 
def reverse_list(l):
    return l[::-1]

s = 'racecar'
s1 = 'not a palindrome'
s3 = 'abba'
s4 = ''

def is_palidrome(s):
    if(len(s) == 0):
        return False
    
    middle = len(s) // 2
    is_even = len(s) % 2 == 0
    
    if(is_even):
        first = s[:middle]
        last = s[middle:]
        return first == reverse_list(last)
    else:
        first = s[:middle + 1]
        last = s[middle:]
        return first == reverse_list(last)
    
# print(is_palidrome(s)) # True
# print(is_palidrome(s1)) # False
# print(is_palidrome(s3)) # True
# print(is_palidrome(s4)) # False


box = """
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
"""

box2 = """
1 1 1 5 -2 0
2 1 0 0 1 0
1 1 1 5 11 9
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
"""

tbt = [
    [1,1,1],
    [0,1,0],
    [1,1,1]
]
tbt1 = [
    [1,1,1],
    [2,1,2],
    [1,1,1]
]

def sum_hour(tbt):
    return sum(tbt[0]) + tbt[1][1] + sum(tbt[2])

# print(sum_hour(tbt)) # 7
# print(sum_hour(tbt1)) # 7

def parse_string(box):
    lines = box.split('\n')
    box_parsed = []
    
    for line in lines:
        if(len(line) == 0):
            continue
        
        parsed_line = []
        for item in line.split():
            parsed_line.append(int(item))
        
        box_parsed.append(parsed_line)
    
    return box_parsed


# print(parse_string(box))

def max_hour(box):
    max_sum = -1
    box_parsed = parse_string(box)
    
    for row in range(len(box_parsed) - 2):
        for col in range(len(box_parsed[row]) - 2):
            row1 = box_parsed[row    ][col:col + 3]
            row2 = box_parsed[row + 1][col:col + 3]
            row3 = box_parsed[row + 2][col:col + 3]
            
            tbt = [row1, row2, row3]
            
            sum_tbt = sum_hour(tbt)
            
            if(sum_tbt > max_sum):
                max_sum = sum_tbt
    
    return max_sum

print(max_hour(box2))
