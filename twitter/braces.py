def validate(chars):
    o2c = {
        '{': '}',
        '[': ']',
        '(': ')'
    }
    openers = o2c.keys()
    closers = o2c.values()

    cur_open = []

    for brace in chars:
        # If brace is open add it to stack of opened
        if brace in openers:
            cur_open.append(brace)
        elif brace in closers:
            # If last opened brace's closer is current brace
            if len(cur_open) != 0 and o2c[cur_open[-1]] == brace:
                cur_open.pop()
            else:
                return 'NO'
    
    # If braces
    if len(cur_open) == 0:
        return 'YES'
    else:
        return 'NO'

def braces(values):
    return [validate(s) for s in values]
