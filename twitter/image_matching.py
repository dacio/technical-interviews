def intersect(grid1, grid2):
    new_grid = []
    
    for row1, row2 in zip(grid1, grid2):
        new_row = []
        
        for cell1, cell2 in zip(row1, row2):
            new_row.append(int(cell1) + int(cell2))
        
        new_grid.append(new_row)
    
    return new_grid

# -1: invalid grup, 0: not group, 1: valid
VALID_GROUP = -1
NOT_GROUP = 0
INVALID_GROUP = 1

def is_group(row, col, grid):
    val = 0
    
    # Make sure indexers are in range
    if (-1 < row < len(grid)) and  (-1 < col < len(grid)):
        val = grid[row][col]
    
    # Zero is fine
    if val == 0:
        return NOT_GROUP
    # One invalidates
    elif val == 1:
        return INVALID_GROUP
    # Two depends on adjacent
    else:
        # Set to zero to prevent backtracking
        grid[row][col] = 0

        # Check if adjacent are grouped
        nearby = [
            is_group(row + 1, col, grid),
            is_group(row - 1 ,col, grid),
            is_group(row, col + 1, grid),
            is_group(row, col - 1, grid)
        ]

        if INVALID_GROUP in nearby:
            return INVALID_GROUP
        else:
            return VALID_GROUP

def countMatches(grid1, grid2):
    inter_grid = intersect(grid1, grid2)
    count = 0

    for row in range(len(inter_grid)):
        for col in range(len(inter_grid[row])):
            if inter_grid[row][col] != 0 and is_group(row, col, inter_grid) == VALID_GROUP:
                count += 1

    return count