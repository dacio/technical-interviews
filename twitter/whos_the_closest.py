# Suddenly started timing out
# def helper(s, q):
#     q_str = s[q]
#     matches = [i for i, c in enumerate(s) if i != q and c == q_str]
    
#     if len(matches) == 0:
#         return -1
#     else:
#         # Find the index whose distance is smallest
#         return min(matches, key=lambda x: abs(x - q))

# This times out too?!
def helper(s, q):
    s_q = s[q]
    len_s = len(s)
    
    for i in range(1, max(len_s - q, q + 1)):
        l_index = q - i
        r_index = q + i
        
        if l_index > -1 and s_q == s[l_index]:
            return l_index
        if r_index < len_s and s_q == s[r_index]:
            return r_index
    return -1

def closest(s, queries):
    return [helper(s, q) for q in queries]
